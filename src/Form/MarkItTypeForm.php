<?php

namespace Drupal\markit\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MarkItTypeForm.
 */
class MarkItTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\markit\Entity\MarkItTypeInterface $markit_type */
    $markit_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $markit_type->label(),
      '#description' => $this->t('Label for the MarkIt type.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $markit_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\markit\Entity\MarkItType::load',
      ],
      '#disabled' => !$markit_type->isNew(),
    ];

    $form['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#default_value' => $markit_type->getPrefix(),
      '#description' => $this->t('Set the prefix of the number. If you want to separate singular and plural forms separate them using the "|" character.'),
    ];

    $form['suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix'),
      '#description' => $this->t('Set the suffix of the number. If you want to separate singular and plural forms separate them using the "|" character.'),
      '#default_value' => $markit_type->getSuffix(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\markit\Entity\MarkItInterface $markit_type */
    $markit_type = $this->entity;
    $status = $markit_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label MarkIt type.', [
          '%label' => $markit_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label MarkIt type.', [
          '%label' => $markit_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($markit_type->toUrl('collection'));
  }

}
