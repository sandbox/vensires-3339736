<?php

namespace Drupal\markit\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\markit\MarkItManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MarkItSettingsForm.
 *
 * @package Drupal\markit\Form
 */
class MarkItSettingsForm extends ConfigFormBase {

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoService;

  /**
   * The markit.manager service.
   *
   * @var \Drupal\markit\MarkItManager
   */
  protected $markItManager;

  /**
   * Constructs a \Drupal\markit\Form\MarkItSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The config.typed service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
   *   The bundle info service.
   * @param \Drupal\markit\MarkItManager $markItManager
   *   The markit manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, EntityTypeBundleInfoInterface $bundle_info_service, MarkItManager $markItManager) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->bundleInfoService = $bundle_info_service;
    $this->markItManager = $markItManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.bundle.info'),
      $container->get('markit.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'markit.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'markit_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('markit.settings');
    $form['target_entities'] = [
      '#type' => 'select',
      '#title' => $this->t('Target entities:'),
      '#required' => FALSE,
      '#description' => $this->t('Select the entity content to allow users like it.'),
      '#default_value' => $config->get('target_entities'),
      '#options' => $this->getTargetEntities(),
      '#multiple' => TRUE,
      '#size' => 15,
      '#suffix' => '<blockquote>' . $this->t('Save the form and the form below will get regenerated based on the new configuration.') . '</blockquote>',
    ];

    if ($config->get('target_entities')) {
      $configuration = $config->get('target_entities_actions');
      $form['target_entities_actions'] = [
        '#type' => 'details',
        '#tree' => TRUE,
        '#title' => $this->t('Actions per entity'),
      ];
      $options = $this->markItManager->getMarkItTypes();
      foreach ($config->get('target_entities') as $key => $label) {
        [$entity_type, $bundle] = explode(':', $key);
        $form['target_entities_actions'][$key] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('@label (@entity_type)', [
            '@label' => $label,
            '@entity_type' => $entity_type,
          ]),
          '#description' => $this->t('Set the actions allowed for this content entity and bundle.'),
          '#default_value' => $configuration[$key] ?? [],
          '#options' => $options,
          '#multiple' => TRUE,
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('markit.settings');

    $config
      ->set('target_entities', $form_state->getValue('target_entities'))
      ->set('target_entities_actions', ($form_state->hasValue('target_entities_actions') ? $form_state->getValue('target_entities_actions') : []))
      ->save();
  }

  /**
   * Return list of entity type and bundles options.
   *
   * @return array
   *   Options list.
   */
  public function getTargetEntities() {
    $excluded = [
      'block_content',
      'crop',
      'h5p_content',
      'likeit',
      'markit',
      'path_alias',
      'shortcut',
      'menu_link_content',
    ];
    $entities = [];
    $entity_type_definitions = \Drupal::entityTypeManager()->getDefinitions();
    foreach ($entity_type_definitions as $definition) {
      /** @var \Drupal\Core\Entity\EntityTypeInterface $definition */
      if ($definition instanceof ContentEntityType && !in_array($definition->id(), $excluded, FALSE)) {
        $entities[] = $definition->id();
      }
    }

    $options = [];
    foreach ($entities as $type) {
      $options[$type] = [];
      $bundles = $this->bundleInfoService->getBundleInfo($type);
      if (!empty($bundles)) {
        $op = [];
        foreach ($bundles as $key => $bundle) {
          $op[$type . ':' . $key] = $bundle['label'] ?? '';
        }

        $options[$type] = $op;
      }

    }
    return $options;
  }

}
