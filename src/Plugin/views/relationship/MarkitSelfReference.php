<?php

namespace Drupal\markit\Plugin\views\relationship;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\relationship\RelationshipPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Relationship handler to return another markit on the same target entity.
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("markit_self_reference")
 */
class MarkitSelfReference extends RelationshipPluginBase {

  /**
   * The markit entity type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $markitTypeStorage;

  /**
   * Constructs a MarkitSelfReference object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $markit_type_storage
   *   The markit_type storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $markit_type_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->markitTypeStorage = $markit_type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('markit_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['markit_type'] = ['default' => []];
    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $markit_types = \Drupal::entityTypeManager()->getStorage('markit_type')->loadMultiple();
    $options = [];
    foreach ($markit_types as $markit_type_id => $markit_type) {
      $options[$markit_type_id] = $markit_type->label();
    }

    $form['markit_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => $options,
      '#default_value' => $this->options['markit_type'] ?? '',
      '#description' => $this->t('Choose which type you wish to relate.'),
      '#required' => TRUE,
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    $markit_type_id = $form_state->getValue(['options', 'markit_type']);
    $form_state->setValue(['options', 'markit_type'], $markit_type_id);
  }

  /**
   * Called to implement a relationship in a query.
   */
  public function query() {
    $tables = current($this->query->tables);
    if (!isset($tables['users_field_data'])) {
      return;
    }
    $this->ensureMyTable();

    $def = $this->definition;
    $def['table'] = 'markit';

    $def['left_table'] = $this->tableAlias;
    $def['left_field'] = 'nid';
    $def['field'] = 'target_entity_id';
    $def['type'] = empty($this->options['required']) ? 'LEFT' : 'INNER';
    $def['extra'][] = [
      'field' => 'uid',
      'left_field' => 'uid',
    ];
    $def['extra'][] = [
      'field' => 'type',
      'value' => $this->options['markit_type'],
    ];
    $def['adjusted'] = TRUE;
    $join = \Drupal::service('plugin.manager.views.join')->createInstance('standard', $def);
    $alias = $def['table'] . '_' . $this->table;
    $this->alias = $this->query->addRelationship($alias, $join, 'markit', $this->relationship);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $markit_type_id = $this->options['markit_type'];
    if ($markit_type = $this->markitTypeStorage->load($markit_type_id)) {
      $dependencies[$markit_type->getConfigDependencyKey()][] = $markit_type->getConfigDependencyName();
    }
    return $dependencies;
  }

}
