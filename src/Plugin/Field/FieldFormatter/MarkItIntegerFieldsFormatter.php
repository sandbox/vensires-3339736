<?php

namespace Drupal\markit\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\IntegerFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'markit_integer_fields_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "markit_integer_fields_formatter",
 *   label = @Translation("MarkIt (Counter)"),
 *   description = @Translation("A formatter specially designed for markit fields."),
 *   field_types = {
 *     "integer",
 *   }
 * )
 */
class MarkItIntegerFieldsFormatter extends IntegerFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $prefix_suffix = $this->getSetting('prefix_suffix');
    $entity_type = $items->getEntity()->getEntityTypeId();
    $entity_id = $items->getEntity()->id();
    $field_name = $items->getFieldDefinition()->getName();

    if (strpos($field_name, 'markit__') !== 0) {
      // Make sure our formatter isn't accidentally used on another field.
      return parent::viewElements($items, $langcode);
    }

    list(,$markit_type_id) = explode('__', $field_name);
    /** @var \Drupal\markit\Entity\MarkItType $markit_type */
    $markit_type = \Drupal::entityTypeManager()->getStorage('markit_type')->load($markit_type_id);
    if (!$markit_type) {
      // If a markit_type entity doesn't exist, fall back to the parent's
      // method to avoid causing a break. Log a message though.
      \Drupal::logger('markit')->warning($this->t('Tried using @formatter formatter but @markit_type_id is not an allowed \Drupal\markit\Entity\MarkItTypeInterface entity.', [
        '@formatter' => 'markit_integer_fields_formatter',
        '@marki_type_id' => $markit_type_id,
      ]));
      return parent::viewElements($items, $langcode);
    }

    foreach ($items as $delta => $item) {
      $prefix_singular = '';
      $prefix_plural = '';
      $suffix_singular = '';
      $suffix_plural = '';
      if ($markit_type->getPrefix()) {
        $prefixes = explode('|', $markit_type->getPrefix());
        $prefix_singular = $prefixes[0];
        $prefix_plural = isset($prefixes[1]) ? $prefixes[1] : $prefix_singular;
        $prefix_singular = "<span class='markit--prefix'>{$prefix_singular}</span>";
        $prefix_plural = "<span class='markit--prefix'>{$prefix_plural}</span>";
      }
      if ($markit_type->getSuffix()) {
        $suffixes = explode('|', $markit_type->getSuffix());
        $suffix_singular = $suffixes[0];
        $suffix_plural = isset($suffixes[1]) ? $suffixes[1] : $suffix_singular;
        $suffix_singular = "<span class='markit--suffix'>{$suffix_singular}</span>";
        $suffix_plural = "<span class='markit--suffix'>{$suffix_plural}</span>";
      }
      $elements[$delta] = [
        'wrapper' => [
          '#type' => 'container',
          '#attributes' => [
            'data-markit-type' => $markit_type_id,
            'data-markit-target-entity-id' => $entity_id,
            'data-markit-target-entity-type' => $entity_type,
          ],
          'value' => [
            '#markup' => "<span class='markit--value'>{$item->value}</span>",
          ]
        ],
      ];

      if ($prefix_suffix) {
        $elements[$delta]['wrapper']['#attributes']['data-markit-prefix'] = $markit_type->getPrefix();
        $elements[$delta]['wrapper']['#attributes']['data-markit-suffix'] = $markit_type->getSuffix();
        $elements[$delta]['wrapper']['value']['#markup'] = \Drupal::translation()->formatPlural($item->value, trim("{$prefix_singular} <span class='markit-value'>1</span> {$suffix_singular}"), trim("{$prefix_plural} <span class='markit-value'>@count</span> {$suffix_plural}"));
      }
    }

    return $elements;
  }
}
