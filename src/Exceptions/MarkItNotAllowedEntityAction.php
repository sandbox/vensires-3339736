<?php

namespace Drupal\markit\Exceptions;

/**
 * Class MarkItNotAllowedEntityAction.
 *
 * @package Drupal\markit\Exceptions
 */
class MarkItNotAllowedEntityAction extends \RuntimeException {

}
