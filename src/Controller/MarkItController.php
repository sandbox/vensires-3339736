<?php

namespace Drupal\markit\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\markit\Entity\MarkItType;
use Drupal\markit\MarkItManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class MarkItController.
 *
 * @package Drupal\markit\Controller
 */
class MarkItController extends ControllerBase {

  const ACTION_MARK = 'mark';
  const ACTION_UNMARK = 'unmark';

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @var \Drupal\markit\MarkItManager
   */
  protected $markItManager;

  /**
   * @var \Drupal\Core\Lock\DatabaseLockBackend|\Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * MarkItController constructor.
   *
   * @param \Drupal\markit\MarkItManager $markItManager
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   */
  public function __construct(MarkItManager $markItManager, EventDispatcherInterface $eventDispatcher, LockBackendInterface $lock) {
    $this->markItManager = $markItManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->lock = $lock;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('markit.manager'),
      $container->get('event_dispatcher'),
      $container->get('lock')
    );
  }

  /**
   * Access check
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check.
   * @param string $entity_type
   *   The entity_type of the entity.
   * @param int $id
   *   The id of the entity.
   * @param \Drupal\markit\Entity\MarkItType $markit_type
   *   The id of the markit_type entity.
   * @param string $action
   *
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Returns the access result.
   */
  public function access(AccountInterface $account, $entity_type, $id, MarkItType $markit_type = NULL, $action = NULL) {
    $actions = [
      self::ACTION_MARK,
      self::ACTION_UNMARK,
    ];
    if ($action && !in_array($action, $actions, FALSE)) {
      return AccessResult::forbidden((string) t('Invalid action provided. Only one of the following is allowed: @actions.', [
        '@actions' => implode(',', $actions),
      ]));
    }

    try {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = \Drupal::entityTypeManager()
        ->getStorage($entity_type)
        ->load($id);
    }
    catch (\Exception $exception) {
      return AccessResult::forbidden($exception->getMessage());
    }

    $markit_type_id = $markit_type ? $markit_type->id() : NULL;
    if ($markit_type_id) {
      $valid = $this->markItManager->markItTypeValidForEntity($markit_type_id, $entity_type, $entity->bundle());
      if (!$valid) {
        return AccessResult::forbidden((string) t('Action @markit_type not allowed for the specified entity.', [
          '@markit_type' => $markit_type->id(),
        ]));
      }
    }

    if (!$entity->access('view')) {
      return AccessResult::forbidden('You cannot act on an entity you do not have access to view.');
    }

    $allowed = !$action || ($action && ($account->hasPermission("{$action} markit entities") || $account->hasPermission("{$action} markit entities:{$markit_type->id()}")));
    if (!$allowed) {
      return AccessResult::forbidden('Not sufficient permissions');
    }

    /**
     * Allow other modules to have a word on whether we should allow access or not.
     */
    $modules = $this->moduleHandler()->getImplementations('markit_access_check');
    foreach ($modules as $module) {
      $function = $module . '_markit_access_check';
      $allowed = $function($account, $entity, $markit_type_id, $action);
      if ($allowed === FALSE) {
        return AccessResult::forbidden("Module {$module} forbids access to {$action} entity {$entity_type} with id {$entity->id()}.");
      }
    }

    return AccessResult::allowed();
  }

  /**
   * Current user liked an entity.
   *
   * @param \Drupal\markit\Entity\MarkItType $markit_type
   *   The type of the markit entity.
   * @param string $entity_type
   *   The type of the entity.
   * @param int $id
   *   The id of the entity.
   * @param int score
   *   The score set (if any).
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns the new number of markings. In case of error, the error message
   *   gets returned.
   */
  public function mark(MarkItType $markit_type, $entity_type, $id, $score = NULL) {
    $lock_cid = implode('__', [
      'markit',
      'mark',
      $markit_type->id(),
      $entity_type,
      $id,
      $this->currentUser()->id(),
    ]);
    if ($this->lock->acquire($lock_cid, 30)) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $this->entityTypeManager()->getStorage($entity_type)->load($id);
      $markit = NULL;
      $markings = $this->markItManager->doMark($markit_type->id(), $entity, $this->currentUser(), $score, $markit);
      $this->moduleHandler()->alter('markit_' . $markit_type->id() . '_marked', $markings, $entity, $markit);
      $this->lock->release($lock_cid);
      return new JsonResponse($markings);
    }
    return new JsonResponse([], JsonResponse::HTTP_CONFLICT);
  }

  /**
   * Current user unmarked an entity.
   *
   * @param \Drupal\markit\Entity\MarkItType $markit_type
   *   The MarkitType entity.
   * @param string $entity_type
   *   The type of the entity.
   * @param int $id
   *   The id of the entity.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns the new number of likes. In case of error, the error message gets
   *   returned.
   */
  public function unmark(MarkItType $markit_type, $entity_type, $id) {
    $lock_cid = implode('__', [
      'markit',
      'unmark',
      $markit_type->id(),
      $entity_type,
      $id,
      $this->currentUser()->id(),
    ]);
    if ($this->lock->acquire($lock_cid, 30)) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $this->entityTypeManager()->getStorage($entity_type)->load($id);
      $markit = NULL;
      $markings = $this->markItManager->doUnmark($markit_type->id(), $entity, $this->currentUser(), $markit);
      $this->moduleHandler()->alter('markit_' . $markit_type->id() . '_unmarked', $markings, $entity, $markit);
      return new JsonResponse($markings);
    }
    return new JsonResponse([], JsonResponse::HTTP_CONFLICT);
  }

  /**
   * Get count of markings of an entity.
   *
   * @param string $entity_type
   *   The type of the entity.
   * @param int $id
   *   The id of the entity.
   * @param \Drupal\markit\Entity\MarkItType $markit_type
   *   The MarkItType entity.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns the number of likes. In case of error, the error message gets
   *   returned.
   */
  public function count($entity_type, $id, MarkItType $markit_type = NULL) {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->entityTypeManager()->getStorage($entity_type)->load($id);
    $markit_type_id = $markit_type ? $markit_type->id() : NULL;
    return new JsonResponse($this->markItManager->getCountsOfEntity($entity, $markit_type_id));
  }

  /**
   * Get users who have marked the entity.
   *
   * @param string $entity_type
   *   The type of the entity.
   * @param int $id
   *   The id of the entity.
   * @param \Drupal\markit\Entity\MarkItType $markit_type
   *   The MarkItType entity.
   * @param bool $user_load
   *   If module "rest" is enabled, the users who have marked the entity will
   *   be loaded and normalized. If you don't want that, set this to FALSE and
   *   only their UIDs will be returned.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns the new number of markings. In case of error, the error message
   *   gets returned.
   */
  public function markUsers($entity_type, $id, MarkItType $markit_type, $user_load = TRUE) {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->entityTypeManager()->getStorage($entity_type)->load($id);
    $markit_type_id = $markit_type->id();
    $uids = $this->markItManager->getUsersMarkEntity($entity, $markit_type_id);
    if ($user_load && $this->moduleHandler()->moduleExists('rest')) {
      $accounts = $this->entityTypeManager()->getStorage('user')->loadMultiple($uids);
      $returned = [];
      foreach ($accounts as $account) {
        $returned[] = \Drupal::service('serializer')->normalize($account, 'json');
      }
      return new JsonResponse($returned);
    }
    return new JsonResponse($uids);
  }

}
