<?php

namespace Drupal\markit\Fields;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Class MarkItMarked.
 *
 * Since this is a computed field related to the currently logged-in user, we
 * have to somehow add cacheability data to the field in a way that JSON:API or
 * other providers will properly process.
 *
 * @see https://www.drupal.org/project/drupal/issues/3252278#comment-14801481
 */
class MarkItMarked extends FieldItemList implements CacheableDependencyInterface {

  use ComputedItemListTrait;

  /**
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected $cacheMetadata = NULL;

  /**
   * {@inheritdoc}
   */
  public function computeValue() {
    /** @var \Drupal\markit\MarkItManager $service */
    $service = \Drupal::service('markit.manager');
    [, $markit_type_id] = explode('__', $this->getName());
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->getEntity();
    $isMarked = $service->isMarked($markit_type_id, $entity);
    $this->list[0] = $this->createItem(0, $isMarked);
    $this->cacheMetadata = new CacheableMetadata();
    $this->cacheMetadata->setCacheContexts($this->getCacheContexts());
    $this->cacheMetadata->setCacheTags($this->getCacheTags());
    $this->cacheMetadata->setCacheMaxAge($this->getCacheMaxAge());
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access = parent::access($operation, $account, TRUE);
    if ($return_as_object) {
      // Here you witness a pure hack. The thing is that JSON:API
      // normalization does not compute cacheable metadata for
      // computed relations like this one.
      /** @see \Drupal\jsonapi\JsonApiResource\ResourceIdentifier */
      /** @see \Drupal\jsonapi\Normalizer\ResourceIdentifierNormalizer */
      // However, thanks to the access check, its result is added
      // as a cacheable dependency to the normalization.
      /** @see \Drupal\jsonapi\Normalizer\ResourceObjectNormalizer::serializeField() */
      $this->ensureComputedValue();
      \assert($this->cacheMetadata instanceof CacheableMetadata);
      $access->addCacheableDependency($this->cacheMetadata);
      return $access;
    }
    return $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['user'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return ['user:' . \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
