<?php

namespace Drupal\markit\Fields;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Class MarkItItemListSingle.
 */
class MarkItItemListSingle extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  public function computeValue() {
    /** @var \Drupal\markit\MarkItManager $service */
    $service = \Drupal::service('markit.manager');
    list(,$markit_type_id) = explode('__', $this->getName());
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->getEntity();

    $value = $service->getCountsOfEntity($entity, $markit_type_id);
    $list[] = $this->createItem(0, [
      'value' => !empty($value) ? $value[$markit_type_id] : 0,
    ]);
    $this->list = $list;
  }

}
