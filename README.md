# Introduction
This module provides a flagging system for Drupal and is somewhat equivalent to the Flag module.

# Configuration
## 1. Create your MarkIt types
Go to `Structure -> MarkIt` and create all the different types of markings you want to use.

## 2. Enable the module for the entities and bundles you want
Go to `User Interface -> MarkIt` and choose from the `Target entities` field the bundles you want to allow any type of marking for.

Then save the form.

## 3. Choose the allowed actions per entity bundle
From the same form as (2) open the `Actions per entity` collapsible fieldset and choose for each entity bundle the MarkIt types you want enabled for each one.

Then save the form and clear your caches.

## 4. Set user permissions
Go to `Users -> Permissions` page and set the permissions you want for each MarkIt type or for each and every `MarkIt` type.

For example, if you want to set that a user has viewed a node, you would like to allow users to `Mark` nodes as `viewed` but not allow them to `Unmark` them.

# Endpoints
The endpoints set below don't require any other core or contrib modules to be present. In some cases though, having the REST module enabled may return a different result.

Read below to better understand the use cases.

> **Notice**
> Don't forget that the `X-CSRF-Token` header's value may be retrieved with a `GET` request to `/session/token`.

> **Important**
> The examples below assume the following `markit_type`s are installed on your website:
> - viewed
> - started
> - read
> - ranked
> - completed

## 1. Mark an entity with a specific type of marking
Marks and entity and returns the number of times the entity has been marked with the same type.

**Path**
```
/markit/mark/{markit_type}/{entity_type}/{id}
```

**Example**
```http request
POST /markit/mark/viewed/node/11 HTTP/1.1
X-CSRF-Token: tugshbPvf8RtO2XBLA1_npaHXgk44_wifjh0fVXyRLc
```

**Response**
```json
{
  "viewed": "59"
}
```

> **Notice**
> If the entity bundle specified isn't configured to support the `markit_type` specified, a `403 Access Denied` HTTP code will be returned.

## 2. Mark an entity with a specific type of marking and a score
Marks and entity with a score and returns the number of times the entity has been marked with the same type.

**Path**
```
/markit/mark/{markit_type}/{entity_type}/{id}/{score}
```

**Example**
```http request
POST /markit/mark/ranked/node/11/5 HTTP/1.1
X-CSRF-Token: tugshbPvf8RtO2XBLA1_npaHXgk44_wifjh0fVXyRLc
```

**Response**
```json
{
  "marked": "60"
}
```

## 3. Unmark an entity with a specific type of marking
Unmarks and entity and returns the updated number of times the entity has been marked with the same type.

**Path**
```
/markit/unmark/{markit_type}/{entity_type}/{id}
```

**Example**
```http request
POST /markit/unmark/viewed/node/11 HTTP/1.1
X-CSRF-Token: tugshbPvf8RtO2XBLA1_npaHXgk44_wifjh0fVXyRLc
```

**Response**
```json
{
  "viewed": "58"
}
```

> **Notice**
> If the entity bundle specified isn't configured to support the `markit_type` specified, a `403 Access Denied` HTTP code will be returned.

## 4. Get all the totals of an entity
Returns the number of times the entity has been marked with any type.

**Path**
```
/markit/marks/count/{entity_type}/{id}
```

**Example**
```http request
GET /markit/marks/count/node/7 HTTP/1.1
```

**Response**
```json
{
  "viewed": "58",
  "started": "43",
  "read": "40",
  "completed": "15",
  "ranked": "5"
}
```

## 5. Get the totals of an entity with a specific type of marking
Returns the number of times the entity has been marked with a specific type.

**Path**
```
/markit/marks/count/{entity_type}/{id}/{markit_type}
```

**Example**
```http request
GET /markit/marks/count/node/7/viewed HTTP/1.1
```

**Response**
```json
{
  "viewed": "58"
}
```

## 6. Get the users who marked an entity with a specific type of marking

**Path**
```
markit/users/mark/{entity_type}/{id}/{markit_type}/{user_load}
```

**Example**
```http request
GET /markit/users/mark/node/7/viewed/0 HTTP/1.1
```

> **Notice**
> `user_load` allowed values are `0` and `1`. If set to 1 and `REST` module is enabled, the response will contain an array with the serialized user accounts who have marked the entity with the specified `markit_type`. Otherwise, an array with their UIDs will be returned. This feature is enabled by default.
